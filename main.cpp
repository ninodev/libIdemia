#include "idemiawidget.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    IdemiaWidget w;
    w.show();

    return a.exec();
}
