#include "idemia.h"

Idemia::Idemia()
{
    gs_uc_RequestCounter = 0;
}

void Idemia::ComputeCrc16(uint8_t *i_puc_Buffer, uint32_t i_ul_Len, uint8_t *io_puc_CrcH, uint8_t *io_puc_CrcL)
{
    static const uint8_t LOOKUP1[ ] = {
        0x00,0x21,0x42,0x63,0x84,0xA5,0xC6,0xE7,
        0x08,0x29,0x4A,0x6B,0x8C,0xAD,0xCE,0xEF,
        0x31,0x10,0x73,0x52,0xB5,0x94,0xF7,0xD6,
        0x39,0x18,0x7B,0x5A,0xBD,0x9C,0xFF,0xDE,
        0x62,0x43,0x20,0x01,0xE6,0xC7,0xA4,0x85,
        0x6A,0x4B,0x28,0x09,0xEE,0xCF,0xAC,0x8D,
        0x53,0x72,0x11,0x30,0xD7,0xF6,0x95,0xB4,
        0x5B,0x7A,0x19,0x38,0xDF,0xFE,0x9D,0xBC,
        0xC4,0xE5,0x86,0xA7,0x40,0x61,0x02,0x23,
        0xCC,0xED,0x8E,0xAF,0x48,0x69,0x0A,0x2B,
        0xF5,0xD4,0xB7,0x96,0x71,0x50,0x33,0x12,
        0xFD,0xDC,0xBF,0x9E,0x79,0x58,0x3B,0x1A,
        0xA6,0x87,0xE4,0xC5,0x22,0x03,0x60,0x41,
        0xAE,0x8F,0xEC,0xCD,0x2A,0x0B,0x68,0x49,
        0x97,0xB6,0xD5,0xF4,0x13,0x32,0x51,0x70,
        0x9F,0xBE,0xDD,0xFC,0x1B,0x3A,0x59,0x78,
        0x88,0xA9,0xCA,0xEB,0x0C,0x2D,0x4E,0x6F,
        0x80,0xA1,0xC2,0xE3,0x04,0x25,0x46,0x67,
        0xB9,0x98,0xFB,0xDA,0x3D,0x1C,0x7F,0x5E,
        0xB1,0x90,0xF3,0xD2,0x35,0x14,0x77,0x56,
        0xEA,0xCB,0xA8,0x89,0x6E,0x4F,0x2C,0x0D,
        0xE2,0xC3,0xA0,0x81,0x66,0x47,0x24,0x05,
        0xDB,0xFA,0x99,0xB8,0x5F,0x7E,0x1D,0x3C,
        0xD3,0xF2,0x91,0xB0,0x57,0x76,0x15,0x34,
        0x4C,0x6D,0x0E,0x2F,0xC8,0xE9,0x8A,0xAB,
        0x44,0x65,0x06,0x27,0xC0,0xE1,0x82,0xA3,
        0x7D,0x5C,0x3F,0x1E,0xF9,0xD8,0xBB,0x9A,
        0x75,0x54,0x37,0x16,0xF1,0xD0,0xB3,0x92,
        0x2E,0x0F,0x6C,0x4D,0xAA,0x8B,0xE8,0xC9,
        0x26,0x07,0x64,0x45,0xA2,0x83,0xE0,0xC1,
        0x1F,0x3E,0x5D,0x7C,0x9B,0xBA,0xD9,0xF8,
        0x17,0x36,0x55,0x74,0x93,0xB2,0xD1,0xF0};

    static const uint8_t LOOKUP2[ ] = {
        0x00,0x10,0x20,0x30,0x40,0x50,0x60,0x70,
        0x81,0x91,0xA1,0xB1,0xC1,0xD1,0xE1,0xF1,
        0x12,0x02,0x32,0x22,0x52,0x42,0x72,0x62,
        0x93,0x83,0xB3,0xA3,0xD3,0xC3,0xF3,0xE3,
        0x24,0x34,0x04,0x14,0x64,0x74,0x44,0x54,
        0xA5,0xB5,0x85,0x95,0xE5,0xF5,0xC5,0xD5,
        0x36,0x26,0x16,0x06,0x76,0x66,0x56,0x46,
        0xB7,0xA7,0x97,0x87,0xF7,0xE7,0xD7,0xC7,
        0x48,0x58,0x68,0x78,0x08,0x18,0x28,0x38,
        0xC9,0xD9,0xE9,0xF9,0x89,0x99,0xA9,0xB9,
        0x5A,0x4A,0x7A,0x6A,0x1A,0x0A,0x3A,0x2A,
        0xDB,0xCB,0xFB,0xEB,0x9B,0x8B,0xBB,0xAB,
        0x6C,0x7C,0x4C,0x5C,0x2C,0x3C,0x0C,0x1C,
        0xED,0xFD,0xCD,0xDD,0xAD,0xBD,0x8D,0x9D,
        0x7E,0x6E,0x5E,0x4E,0x3E,0x2E,0x1E,0x0E,
        0xFF,0xEF,0xDF,0xCF,0xBF,0xAF,0x9F,0x8F,
        0x91,0x81,0xB1,0xA1,0xD1,0xC1,0xF1,0xE1,
        0x10,0x00,0x30,0x20,0x50,0x40,0x70,0x60,
        0x83,0x93,0xA3,0xB3,0xC3,0xD3,0xE3,0xF3,
        0x02,0x12,0x22,0x32,0x42,0x52,0x62,0x72,
        0xB5,0xA5,0x95,0x85,0xF5,0xE5,0xD5,0xC5,
        0x34,0x24,0x14,0x04,0x74,0x64,0x54,0x44,
        0xA7,0xB7,0x87,0x97,0xE7,0xF7,0xC7,0xD7,
        0x26,0x36,0x06,0x16,0x66,0x76,0x46,0x56,
        0xD9,0xC9,0xF9,0xE9,0x99,0x89,0xB9,0xA9,
        0x58,0x48,0x78,0x68,0x18,0x08,0x38,0x28,
        0xCB,0xDB,0xEB,0xFB,0x8B,0x9B,0xAB,0xBB,
        0x4A,0x5A,0x6A,0x7A,0x0A,0x1A,0x2A,0x3A,
        0xFD,0xED,0xDD,0xCD,0xBD,0xAD,0x9D,0x8D,
        0x7C,0x6C,0x5C,0x4C,0x3C,0x2C,0x1C,0x0C,
        0xEF,0xFF,0xCF,0xDF,0xAF,0xBF,0x8F,0x9F,
        0x6E,0x7E,0x4E,0x5E,0x2E,0x3E,0x0E,0x1E};
    uint8_t	l_uc_Indice;
    uint8_t	l_uc_L    = *io_puc_CrcL;
    uint8_t	l_uc_H    = *io_puc_CrcH;
    uint8_t* l_puc_End = i_puc_Buffer + i_ul_Len;

    for ( ; i_puc_Buffer < l_puc_End ; )
    {
        l_uc_Indice = static_cast<uint8_t>( l_uc_H ^ * (i_puc_Buffer++) );
        l_uc_H      = static_cast<uint8_t>( LOOKUP2[l_uc_Indice] ^ l_uc_L );
        l_uc_L       = LOOKUP1[l_uc_Indice];
    }

    *io_puc_CrcL = l_uc_L;
    *io_puc_CrcH = l_uc_H;

    return;

}

int32_t Idemia::SPRS232_Initialize(int8_t *i_pc_Name, uint32_t i_ul_BaudRate)
{
    int32_t	l_i_Ret;

        l_i_Ret = RS232_Initialize( i_pc_Name,
                                     i_ul_BaudRate );

        if ( l_i_Ret )
            return l_i_Ret;


        return SPRS232_OK;

}

int32_t Idemia::SPRS232_Break(uint32_t i_ul_Time)
{
    return RS232_Break(i_ul_Time);
}

int32_t Idemia::SPRS232_Close()
{
    int32_t	l_i_Ret;

    l_i_Ret = RS232_Close( );
    if ( l_i_Ret )
        return l_i_Ret;

    return SPRS232_OK;

}

int32_t Idemia::SPRS232_Send(uint8_t *i_puc_Data, uint32_t i_ul_DataSize)
{
    uint32_t		l_ul_SizeOfSendData = 0;
        uint32_t		l_ul_Count          = 0;
        uint8_t		l_uc_Attempt;
        int32_t		l_i_Status;
        uint8_t		l_uc_ID;
        uint8_t l_b_First;
        uint8_t	l_b_Last;
        uint8_t		l_uc_RC;
        uint8_t		l_uc_MaxNumberOfAttempt = 3;
        uint32_t		l_ul_PacketCurrentSize  = 0;
        uint8_t		l_auc_PacketToSend[2058];		// 1 (RC) + 1024 (data) + 2 (CRC) = 1027,
                                                // X 2 for stuffing, + 4 (STX ID and DLE ETX) = 2058
        // Receive loop
        do
        {
            // Cut data in packets size <= MAX_DATA_SIZE
            l_ul_SizeOfSendData = MIN(MAX_DATA_SIZE, (i_ul_DataSize - l_ul_Count));

            l_uc_Attempt = 0;

            // Add SOP
            s_SPRS232_MakeSOP( SPRS232_ID_ID_PACKET_DATA,		// Packet type
                               static_cast<uint8_t>(l_ul_Count==0?1:0),	// FIRST
                               static_cast<uint8_t>((i_ul_DataSize-l_ul_Count-l_ul_SizeOfSendData)==0?1:0), // LAST
                               gs_uc_RequestCounter,
                               l_auc_PacketToSend,				// Pointer on the beginning of the packet
                              &l_ul_PacketCurrentSize );		// Position where to write in the packet

            // Add data and CRC + stuffing
            s_SPRS232_AddDataToPacket( l_auc_PacketToSend,
                                      &l_ul_PacketCurrentSize,
                                       i_puc_Data+l_ul_Count,
                                       l_ul_SizeOfSendData );

            // Add EOP
            s_SPRS232_AddEOP( l_auc_PacketToSend,
                             &l_ul_PacketCurrentSize );

            do
            {
                // Send data
                l_i_Status = RS232_Write( l_auc_PacketToSend,
                                          l_ul_PacketCurrentSize,
                                            nullptr);
                if ( l_i_Status )
                    return l_i_Status;

                // Wait acquitment for the sent packet (wait still there is no error)
                do
                {
                    l_i_Status = s_SPRS232_ReceiveSOP( &l_uc_ID,
                                                       &l_b_First,
                                                       &l_b_Last,
                                                       &l_uc_RC,
                                                       ACK_TIMEOUT );

                }
                while ( ( ! l_i_Status ) && ( gs_uc_RequestCounter != l_uc_RC ) );

                l_uc_Attempt++;

                if ( l_i_Status != SPRS232_OK )
                {
                    continue;
                }
                else if ( l_uc_ID == SPRS232_ID_ID_PACKET_ACK )
                {
                    gs_uc_RequestCounter++;
                    break;
                }
                else if ( l_uc_ID == SPRS232_ID_ID_PACKET_NACK )
                {
                    // If NACK is received, number of receive attempt is 5
                    l_uc_MaxNumberOfAttempt = 5;
                    continue;
                }
                else
                {
                    continue;
                }

            }
            while ( l_uc_Attempt < l_uc_MaxNumberOfAttempt );

            if ( l_uc_Attempt >= l_uc_MaxNumberOfAttempt )
            {
                gs_uc_RequestCounter++;

                return l_i_Status;
            }

            l_ul_Count += l_ul_SizeOfSendData;	// Nb of bytes sent

        }
        while ( i_ul_DataSize-l_ul_Count >0 );

        return SPRS232_OK;
}

int32_t Idemia::SPRS232_Receive(uint8_t *i_puc_Buffer, uint32_t i_ul_BufferSize, uint32_t *o_pul_ILVSize, uint32_t i_ul_ReadTotalTimeoutConstant)
{
    uint8_t		l_uc_Id;
        int32_t		l_i_Status;
        int32_t		l_i_Status2;
        uint32_t		l_ul_PacketSize;
        uint8_t l_b_Last;
        uint8_t		l_uc_First;
        uint8_t		l_uc_RC;
        uint8_t		l_uc_ACK_RC;
        uint8_t		l_auc_PacketToSend[4];	// Paquet for ACK or NACK (3 octets + 1 if stuffing on RC)
        uint32_t		l_ul_PacketCurrentSize;
        uint8_t l_b_Start;
        uint32_t		l_ul_BufferSize;

        // Initialization
        l_b_Last    = 0;
        l_uc_ACK_RC =  0xFF;
        l_b_Start   = 0;

        // Receive loop
        do
        {
            l_ul_BufferSize = i_ul_BufferSize - *o_pul_ILVSize;

            do
            {
                // Receive SOP data with timeout gien in parameters
                l_i_Status = s_SPRS232_ReceiveSOP(&l_uc_Id,
                                                  &l_uc_First,
                                                  &l_b_Last,
                                                  &l_uc_RC,
                                                   i_ul_ReadTotalTimeoutConstant );

                if ( l_i_Status != SPRS232_OK )
                    return l_i_Status;

                if ( l_uc_First == SPRS232_ID_TYP_PACKET_FIRST )
                {
                    *o_pul_ILVSize = 0;
                    l_b_Start = 1;
                }
            }
            while ( l_uc_Id != SPRS232_ID_ID_PACKET_DATA || l_b_Start == 0 );

            // Receive the packet data
            l_i_Status = s_SPRS232_ReceivePacket( i_puc_Buffer+*o_pul_ILVSize,
                                                 &l_ul_PacketSize,
                                                  l_ul_BufferSize );

            // When the buffer is too small, a ACK is sent so that the MSO does not send several packets
            // (to avoid desynchronization with PC at next reception)
            if ( l_i_Status == SPRS232_OK || l_i_Status == SPRS232_ERR_SMALL_BUF )
            {
                // Check that a ACK has not already been sent for this packet
                if ( ( l_uc_ACK_RC == l_uc_RC ) &&( ! l_uc_First ) )
                    continue;

                // Send ACK
                s_SPRS232_MakeSOP( SPRS232_ID_ID_PACKET_ACK,
                                   1,
                                   1,
                                   l_uc_RC,
                                   l_auc_PacketToSend,
                                  &l_ul_PacketCurrentSize );

                l_i_Status2 = RS232_Write( l_auc_PacketToSend,
                                           l_ul_PacketCurrentSize,
                                           nullptr);

                if ( l_i_Status2 != SPRS232_OK )
                {
                    return l_i_Status;
                }

                l_uc_ACK_RC     = l_uc_RC; // Last packet
                *o_pul_ILVSize += l_ul_PacketSize;

                if ( l_i_Status == SPRS232_ERR_SMALL_BUF )
                    return l_i_Status;
            }
            else if ( l_i_Status != RS232ERR_READ_TIMEOUT )
            {
                // Send NACK
                s_SPRS232_MakeSOP( SPRS232_ID_ID_PACKET_NACK,
                                   1,
                                   1,
                                   l_uc_RC,
                                   l_auc_PacketToSend,
                                  &l_ul_PacketCurrentSize );

                l_i_Status = RS232_Write( l_auc_PacketToSend,
                                          l_ul_PacketCurrentSize,
                                          nullptr);

                if ( l_i_Status != SPRS232_OK )
                {
                    return l_i_Status;
                }

                l_b_Last = 0;
            }
            else
                return l_i_Status;
        }
        while ( ! l_b_Last );

        return SPRS232_OK;
}

void Idemia::s_SPRS232_MakeSOP(uint8_t l_uc_Type, uint8_t l_b_First, uint8_t l_b_Last, uint8_t i_uc_RC, uint8_t *o_puc_PacketToSend, uint32_t *o_pul_PacketCurrentSize)
{
    uint8_t	l_uc_Byte;
    // Add STX
    o_puc_PacketToSend[0] = STX;
    // Add ID
    l_uc_Byte = l_uc_Type | (DIR_OUT&SPRS232_ID_TYP_PACKET_DIR);

    if ( l_b_First )
        l_uc_Byte |= SPRS232_ID_TYP_PACKET_FIRST;

    if ( l_b_Last )
        l_uc_Byte |= SPRS232_ID_TYP_PACKET_LAST;

    o_puc_PacketToSend[1] = l_uc_Byte;

    // Stuffing on RC and add of RC
    switch ( i_uc_RC )
    {
    case DLE :
        o_puc_PacketToSend[2] = DLE;
        o_puc_PacketToSend[3] = DLE;
        *o_pul_PacketCurrentSize = 4;
        break;

    case XON :
        o_puc_PacketToSend[2] = DLE;
        o_puc_PacketToSend[3] = XON+1;
        *o_pul_PacketCurrentSize = 4;
        break;

    case XOFF :
        o_puc_PacketToSend[2] = DLE;
        o_puc_PacketToSend[3] = XOFF+1;
        *o_pul_PacketCurrentSize = 4;
        break;

    default :
        o_puc_PacketToSend[2] = i_uc_RC;
        *o_pul_PacketCurrentSize = 3;
    }
    return;
}

void Idemia::s_SPRS232_AddDataToPacket(uint8_t *io_puc_PacketToSend, uint32_t *io_pul_PacketCurrentSize, uint8_t *i_puc_Data, uint32_t i_ul_SizeOfSendData)
{
    uint32_t	l_ul_i;
    uint8_t	l_auc_CRC[2] = {0x00, 0x00};

    // Compute CRC
    ComputeCrc16( i_puc_Data,
                  i_ul_SizeOfSendData,
                  l_auc_CRC+1,
                  l_auc_CRC );

    // Add data doing stuffing
    for ( l_ul_i = 0 ; l_ul_i < i_ul_SizeOfSendData ; l_ul_i++ )
    {
        switch ( i_puc_Data[l_ul_i] )
        {
        case DLE :
            io_puc_PacketToSend[(*io_pul_PacketCurrentSize)++] = DLE;
            io_puc_PacketToSend[(*io_pul_PacketCurrentSize)++] = DLE;
            break;

        case XON :
            io_puc_PacketToSend[(*io_pul_PacketCurrentSize)++] = DLE;
            io_puc_PacketToSend[(*io_pul_PacketCurrentSize)++] = XON+1;
            break;

        case XOFF :
            io_puc_PacketToSend[(*io_pul_PacketCurrentSize)++] = DLE;
            io_puc_PacketToSend[(*io_pul_PacketCurrentSize)++] = XOFF+1;
            break;

        default :
            io_puc_PacketToSend[(*io_pul_PacketCurrentSize)++] = i_puc_Data[l_ul_i];
            break;
        }
    }

    // Add CRC doing stuffing
    for ( l_ul_i = 0 ; l_ul_i < 2 ; l_ul_i++ )
    {
        switch ( l_auc_CRC[l_ul_i] )
        {
        case DLE:
            io_puc_PacketToSend[(*io_pul_PacketCurrentSize)++] = DLE;
            io_puc_PacketToSend[(*io_pul_PacketCurrentSize)++] = DLE;
            break;

        case XON:
            io_puc_PacketToSend[(*io_pul_PacketCurrentSize)++] = DLE;
            io_puc_PacketToSend[(*io_pul_PacketCurrentSize)++] = XON+1;
            break;

        case XOFF:
            io_puc_PacketToSend[(*io_pul_PacketCurrentSize)++] = DLE;
            io_puc_PacketToSend[(*io_pul_PacketCurrentSize)++] = XOFF+1;
            break;

        default:
            io_puc_PacketToSend[(*io_pul_PacketCurrentSize)++] = l_auc_CRC[l_ul_i];
            break;
        }
    }
    return;
}

void Idemia::s_SPRS232_AddEOP(uint8_t *io_puc_PacketToSend, uint32_t *io_pul_PacketCurrentSize)
{
    io_puc_PacketToSend[(*io_pul_PacketCurrentSize)++] = DLE;
    io_puc_PacketToSend[(*io_pul_PacketCurrentSize)++] = ETX;

    return;

}

int Idemia::s_SPRS232_UnStuffing(uint8_t *o_puc_Data)
{
    switch ( *o_puc_Data )
    {
    case DLE :
        break;

    case XON+1 :
        *o_puc_Data = XON;	// <XON>
        break;

    case XOFF+1 :
        *o_puc_Data = XOFF;	// <XOFF>
        break;

    case ETX:
        return SPRS232_ERR_EOP;

    default:
        return SPRS232_BAD_STUFFING;
    }

    return SPRS232_OK;

}

int Idemia::s_SPRS232_ReceiveSOP(uint8_t *o_uc_Type, uint8_t *o_b_First, uint8_t *o_b_Last, uint8_t *o_puc_RC, uint32_t i_ul_ReadTotalTimeoutConstant)
{
    uint8_t		l_uc_Byte;
    uint32_t		l_i_State;
    uint32_t	l_ul_Timeout;
    uint32_t		l_i_NbErr;
    int32_t		l_i_Status;

    // Initialization
    *o_uc_Type   = 0xFF;
    *o_b_First   = 0xFF;
    *o_b_Last    = 0xFF;
    *o_puc_RC    = 0xFF;
    l_uc_Byte    = 0xFF;
    l_i_State	 = SPRS232_WAIT_STX;
    l_ul_Timeout = i_ul_ReadTotalTimeoutConstant;
    l_i_NbErr	 = 0;

    // Receive SOP loop
    while ( l_i_State != SPRS232_WAIT_DATA )
    {
        l_i_Status = RS232_Read(&l_uc_Byte, 1, l_ul_Timeout);

        if ( l_i_Status )
        {
            if ( l_i_Status == RS232ERR_READ_TIMEOUT )
            {
                if ( l_i_NbErr )
                {
                    // Several errors occured
                    return SPRS232_ERR_ERROR;
                }
                else
                {
                    // First character never arrived
                    return l_i_Status;
                }
            }

            l_i_NbErr++;
            l_i_State	 = SPRS232_WAIT_STX;
            l_ul_Timeout = static_cast<uint32_t>(SPRS232_TIMEOUT_ACK*1.5);
        }
        else
        {
            switch ( l_i_State )
            {
            case SPRS232_WAIT_STX :
                if ( l_uc_Byte == STX )
                {
                    l_i_State    = SPRS232_WAIT_ID;
                    l_ul_Timeout = 0;
                }
                break;

            case SPRS232_WAIT_ID :
                if ( ( ( l_uc_Byte & SPRS232_ID_TYP_PACKET_DIR ) == DIR_IN )
                     &&
                     (
                         ( ( l_uc_Byte & SPRS232_MASK_PACKET_TYPE ) == SPRS232_ID_ID_PACKET_DATA )
                         |
                         ( ( l_uc_Byte & SPRS232_MASK_PACKET_TYPE ) == SPRS232_ID_ID_PACKET_ACK  )
                         |
                         ( ( l_uc_Byte & SPRS232_MASK_PACKET_TYPE ) == SPRS232_ID_ID_PACKET_NACK )
                         )
                     )
                {
                    *o_b_First = l_uc_Byte & SPRS232_ID_TYP_PACKET_FIRST;
                    *o_b_Last  = l_uc_Byte & SPRS232_ID_TYP_PACKET_LAST;
                    *o_uc_Type = l_uc_Byte & SPRS232_MASK_PACKET_TYPE;
                    l_i_State  = SPRS232_WAIT_RC;
                }
                else
                {
                    l_i_State	 = SPRS232_WAIT_STX;
                    l_ul_Timeout = SPRS232_TIMEOUT_ACK;
                }
                break;

            case SPRS232_WAIT_RC :

                if ( l_uc_Byte == DLE )
                {
                    l_i_State = SPRS232_WAIT_RC_AFTER_DLE;
                }
                else
                {
                    * o_puc_RC = l_uc_Byte;
                    l_i_State  = SPRS232_WAIT_DATA;
                }
                break;

            case SPRS232_WAIT_RC_AFTER_DLE :
                l_i_Status = s_SPRS232_UnStuffing( &l_uc_Byte );

                if ( l_i_Status )
                {
                    l_i_State	 = SPRS232_WAIT_STX;
                    l_ul_Timeout = SPRS232_TIMEOUT_ACK;
                }
                else
                {
                    * o_puc_RC = l_uc_Byte;
                    l_i_State  = SPRS232_WAIT_DATA;
                }
                break;

            default :
                return RS232ERR_ERROR;
            }
        }
    }

    return SPRS232_OK;

}

int Idemia::s_SPRS232_ReadOneByte(uint8_t *o_puc_Data)
{
    int32_t	l_i_Status;

        l_i_Status = RS232_Read( o_puc_Data,
                                 1,
                                 0 );

        if ( l_i_Status )
            return l_i_Status;

        if ( *o_puc_Data == DLE )
        {
            l_i_Status = RS232_Read( o_puc_Data,
                                     1,
                                     0 );

            if ( l_i_Status )
                return l_i_Status;

            l_i_Status = s_SPRS232_UnStuffing( o_puc_Data );

            if ( l_i_Status )
                return l_i_Status;
        }

        return SPRS232_OK;
}

int Idemia::s_SPRS232_ReceivePacket(uint8_t *o_puc_Data, uint32_t *o_pul_Size, uint32_t i_ul_BufferSize)
{
    uint8_t	l_uc_CRCH = 0;		// MSB CRC16
        uint8_t	l_uc_CRCL = 0;		// LSB CRC16
        uint8_t	l_uc_CRCH_Mess = 0;	// MSB CRC16
        uint8_t	l_uc_CRCL_Mess = 0;	// LSB CRC16
        int32_t	l_i_Status;
        int32_t	l_i_BytesReadILV;

        // Initialization
        *o_pul_Size      = 0;
        l_i_BytesReadILV = -1;

        // Receive loop
        do
        {
            l_i_BytesReadILV++;

            if ( l_i_BytesReadILV >= static_cast<int32_t>(i_ul_BufferSize) )
                return SPRS232_ERR_SMALL_BUF;

            l_i_Status = s_SPRS232_ReadOneByte( o_puc_Data+l_i_BytesReadILV );
        }
        while ( ! l_i_Status );

        if ( l_i_Status != SPRS232_ERR_EOP )
            return l_i_Status;

        l_uc_CRCH_Mess = o_puc_Data[--l_i_BytesReadILV];
        l_uc_CRCL_Mess = o_puc_Data[--l_i_BytesReadILV];

        *o_pul_Size = static_cast<uint32_t>(l_i_BytesReadILV);

        // Check CRC
        ComputeCrc16( static_cast<uint8_t *>(o_puc_Data),
                      static_cast<uint32_t>(*o_pul_Size),
                     &l_uc_CRCH,
                     &l_uc_CRCL );

        if ( ( l_uc_CRCL_Mess != l_uc_CRCL ) || ( l_uc_CRCH_Mess != l_uc_CRCH ) )
        {
            *o_pul_Size = 0;
            return SPRS232_ERR_CRC;
        }

        return SPRS232_OK;
}

int32_t Idemia::RS232_Initialize(int8_t *i_pc_Name, uint32_t i_ul_BaudRate)
{

}

int32_t Idemia::RS232_Close()
{

}

int32_t Idemia::RS232_Write(uint8_t *i_puc_Buf, uint32_t i_ul_Size, uint32_t *o_pul_BytesWritten)
{

}

int32_t Idemia::RS232_Read(uint8_t *i_puc_Buf, uint32_t i_ul_Size, uint32_t i_ul_ReadTotalTimeoutConstant)
{

}

int32_t Idemia::RS232_Break(uint32_t i_ul_Time)
{

}

