#ifndef IDEMIAWIDGET_H
#define IDEMIAWIDGET_H

#include <QWidget>

namespace Ui {
class IdemiaWidget;
}

class IdemiaWidget : public QWidget
{
    Q_OBJECT

public:
    explicit IdemiaWidget(QWidget *parent = nullptr);
    ~IdemiaWidget();

private:
    Ui::IdemiaWidget *ui;
};

#endif // IDEMIAWIDGET_H
