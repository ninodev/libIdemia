#ifndef IDEMIA_H
#define IDEMIA_H
#include <stdint.h>
//from SpIlv.h
#define ILV_GetI(i_puc_ILV)	((uint8_t)*i_puc_ILV)
//from Sp_Rs232.h
#define MAX_DATA_SIZE				1024
#define ACK_TIMEOUT					1000
//from Ilv.c
#define GET_US(i_p_UC)  ( (uint16_t) (((uint16_t)*( (uint8_t *)(i_p_UC)    +1))<<8  ) | (*((uint8_t *)(i_p_UC))) )
#define GET_UL(i_puc_US)( (UL) (((UL) (GET_US((i_puc_US)+2))<<16 ) | (UL)(GET_US(i_puc_US) ) ) )
#define SET_US(i_p_UC,i_US)( (*(i_p_UC)=(UC)(i_US)),(*(i_p_UC+1)=(UC)((i_US)>>8)) )
#define SET_UL(i_p_UC,i_UL)( SET_US(i_p_UC,(US)i_UL), SET_US(i_p_UC+2,(US)((i_UL)>>16)))

//from Sp_Rs232.c
#ifndef MAX
    #define MAX(a,b) ((( a )>( b )) ? ( a ):( b ))
#endif
#ifndef MIN
    #define MIN(a,b) ((( a )<( b )) ? ( a ):( b ))
#endif

class Idemia
{
public:
    Idemia();
    ~Idemia();
private:
    uint8_t gs_uc_RequestCounter;
    // From Errors.h
    static constexpr int32_t RETURN_NO_ERROR = 0;
    static constexpr int32_t SPILVERR_BAD_PARAMETER = -206;
    static constexpr int32_t ILV_ERROR_INVALID_ILV = -600;          /*0xDA8*/
    static constexpr int32_t ILV_ERROR_BAD_ILV_SIZE = -601;         /*0xDA7*/
    static constexpr int32_t ILV_ERROR_NOT_ENOUGH_MEMORY = -602;	/*0xDA6*/
    static constexpr int32_t ILVERR_CARD_NOT_FOUND = -603;          /*0xDA5*/
    static constexpr int32_t ILVERR_COM_ERROR = -604;               /*0xDA4*/

    // General commands

    static constexpr int32_t ILV_GET_DESCRIPTOR = 0x05;
    static constexpr int32_t ILV_GET_BASE_CONGIG = 0x07;

    // Biometric commands

    static constexpr int32_t ILV_VERIFY = 0x20;
    static constexpr int32_t ILV_ENROLL = 0x21;
    static constexpr int32_t ILV_IDENTIFY = 0x22;
    static constexpr int32_t ILV_VERIFY_MATCH = 0x23;
    static constexpr int32_t ILV_IDENTIFY_MATCH = 0x24;

    // DataBase commands

    static constexpr int32_t ILV_CREATE_DB = 0x30;
    static constexpr int32_t ILV_ERASE_BASE = 0x32;
    static constexpr int32_t ILV_DESTROY_ALL_BASE = 0x33;
    static constexpr int32_t ILV_ERASE_ALL_BASE = 0x34;
    static constexpr int32_t ILV_ADD_RECORD	= 0x35;
    static constexpr int32_t ILV_REMOVE_RECORD = 0x36;
    static constexpr int32_t ILV_FIND_USER_DB = 0x38;
    static constexpr int32_t ILV_DESTROY_DB = 0x3B;
    static constexpr int32_t ILV_LIST_PUBLIC_FIELDS = 0x3E;
    static constexpr int32_t ILV_GET_DATA_DB = 0x3F;

    // Miscellaneous

    static constexpr int32_t ILV_CONFIG_UART = 0xEE;
    static constexpr int32_t ILV_ASYNC_MESSAGE = 0x71;

    // Invalid ILV

    static constexpr int32_t ILV_INVALID = 0x50;

    // Error codes

    static constexpr int32_t ILV_OK	= 0x00;	//  0	Successful result
    static constexpr int32_t ILVERR_ERROR = 0xFF;	// -1	An error occurred
    static constexpr int32_t ILVERR_BADPARAMETER = 0xFE;	// -2	Input parameters are not valid
    static constexpr int32_t ILVERR_INVALID_MINUTIAE = 0xFD;	// -3	The minutiae is not valid
    static constexpr int32_t ILVERR_INVALID_USER_ID = 0xFC;	// -4	The record identifier does not exist in the database
    static constexpr int32_t ILVERR_INVALID_USER_DATA = 0xFB;	// -5	The user data are not valid
    static constexpr int32_t ILVERR_TIMEOUT = 0xFA;	// -6	No response after defined time.
    static constexpr int32_t ILVERR_ALREADY_ENROLLED = 0xF8;	// -8	The person is already in the base
    static constexpr int32_t ILVERR_BASE_NOT_FOUND = 0xF7;	// -9	The specified base does not exist
    static constexpr int32_t ILVERR_BASE_ALREADY_EXISTS = 0xF6;	//-10	The specified base already exist
    static constexpr int32_t ILVERR_BIO_IN_PROGRESS = 0xF5;	//-11	Command received during biometric processing
    static constexpr int32_t ILVERR_FLASH_INVALID = 0xF3;	//-13	Flash type invalid
    static constexpr int32_t ILVERR_NO_SPACE_LEFT = 0xF2;	//-14	Not Enough memory for the creation of a database
    static constexpr int32_t ILVERR_BAD_SIGNATURE = 0xF0;	//-16	Invalid digital signature
    static constexpr int32_t ILVERR_OUT_OF_FIELD = 0xEB;	//-21
    static constexpr int32_t ILVERR_FIELD_NOT_FOUND = 0xE9;	//-23	Field does not exist.
    static constexpr int32_t ILVERR_FIELD_INVALID = 0xE8;	//-24	Field size or field name is invalid.
    static constexpr int32_t ILVERR_USER_NOT_FOUND = 0xE6;	//-26	The searched user is not found.
    static constexpr int32_t ILVERR_CMDE_ABORTED = 0xE5;	//-27	Commanded has been aborted by the user.
    static constexpr int32_t ILVERR_SAME_FINGER = 0xE4;	//-28	There are two templates of the same finger
    static constexpr int32_t ILVERR_NO_HIT = 0xE3;	//-29	Presented finger does not match
    static constexpr int32_t ILVERR_SECU_CERTIF_NOT_EXIST = 0xE2;	//-30	The required certificate does not exist
    static constexpr int32_t ILVERR_SECU_BAD_STATE = 0xE1;	//-31	Invalid security state
    static constexpr int32_t ILVERR_SECU_ANTIREPLAY = 0xE0;	//-32	An antireplay error occured
    static constexpr int32_t ILVERR_SECU_ASN1 = 0xDF;	//-33	Error while decoding an ASN1 object
    static constexpr int32_t ILVERR_SECU = 0xDE;	//-34	Cryptographic error
    static constexpr int32_t ILVERR_SECU_AUTHENTICATION = 0xDD;	//-35	Mutual authentication errors
    static constexpr int32_t ILVERR_NOT_IMPLEMENTED = 0x9D;	//-99	The request is not yet implemented.

    // Status codes

    static constexpr int32_t ILVSTS_OK = 0;	//0x00	Successful
    static constexpr int32_t ILVSTS_HIT = 1;	//0x01	Authentication or Identification succeed
    static constexpr int32_t ILVSTS_NO_HIT = 2;	//0x02	Authentication or Identification failed
    static constexpr int32_t ILVSTS_LATENT = 3;	//0x03	Security Protection Triggered
    static constexpr int32_t ILVSTS_DB_FULL = 4;	//0x04	The database is full.
    static constexpr int32_t ILVSTS_DB_EMPTY = 5;	//0x05	The database is empty.
    static constexpr int32_t ILVSTS_DB_OK = 7;	//0x07	The database is right.
    static constexpr int32_t ILVSTS_DB_KO = 10;	//0x0A	The flash can not be accessed

    // Constants

    static constexpr int32_t ID_PKCOMP = 2;	//0x02	// PKCOMP (minutiae)
    static constexpr int32_t ID_PKMAT = 3;	//0x03	// PKMAT  (minutiae)
    static constexpr int32_t ID_USER_ID = 4;	//0x04	// User ID
    static constexpr int32_t ID_USER_DATA = 5;	//0x05	// User data
    static constexpr int32_t ID_COM1 = 6;	//0x06	// Identify the first serial link of the terminal
    static constexpr int32_t ID_FIELD = 15;	//0x0F
    static constexpr int32_t ID_FIELD_SIZE = 16;	//0x10
    static constexpr int32_t ID_TIME_STAMP = 17;	//0x11
    static constexpr int32_t ID_PUC_DATA = 20;	//0x14	// General data
    static constexpr int32_t ID_DESC_PRODUCT = 41;	//0x29	// Product descriptor
    static constexpr int32_t ID_DESC_SOFTWARE = 42;	//0x2A	// Software descriptor
    static constexpr int32_t ID_DESC_SENSOR = 43;	//0x2B	// Sensor descriptor
    static constexpr int32_t ID_COMPRESSION_NULL = 44;	//0x2C	// No compression is used
    static constexpr int32_t ID_FORMAT_TEXT = 47;	//0x2F	// text format for descriptor
    static constexpr int32_t ID_PRIVATE_FIELD = 49;	//0x31	// Database private field
    static constexpr int32_t ID_FIELD_CONTENT = 50;	//0x32
    static constexpr int32_t ID_ASYNCHRONOUS_EVENT = 52;	//0x34	// Reception of asynchronous events during live finger acquisition
    static constexpr int32_t ID_PKMAT_NORM = 53;	//0x35
    static constexpr int32_t ID_USER_INDEX = 54;  //0x36
    static constexpr int32_t ID_PKCOMP_NORM = 55;	//0x37
    static constexpr int32_t ID_BIO_ALGO_PARAM = 56;	//0x38	// Used to define specific biometric parameter like coder rev., normalization, matcher rev.
    static constexpr int32_t ID_FIND_USER_DATA = 57;	//0x39	// Identifies a pattern to find
    static constexpr int32_t ID_PKBASE = 58;	//0x3A
    static constexpr int32_t ID_EXPORT_IMAGE = 61;	//0x3D
    static constexpr int32_t ID_COMPRESSION = 62;	//0x3E
    static constexpr int32_t ID_X509_CERTIFICATE = 80;	//0x50	// X509 certificate
    static constexpr int32_t ID_PKCS12_TOKEN = 81;	//0x51	// PKCS#12 token
    static constexpr int32_t ID_CRYPTOGRAM_MUTUAL_AUTH = 82;	//0x52	// Cryptogram exchanged during mutual authentication (secure protocol option)
    static constexpr int32_t ID_TKB_SETTINGS = 83;	//0x53
    static constexpr int32_t ID_X984_PARAM = 84;	//0x54
    static constexpr int32_t ID_X984 = 85;	//0x55
    static constexpr int32_t ID_MATCHING_SCORE = 86;	//0x56
    static constexpr int32_t ID_TKB = 87;	//0x57

    //from Sp_Rs232_Internal
    static constexpr int32_t SPRS232_WAIT_STX = 0;
    static constexpr int32_t SPRS232_WAIT_ID = 1;
    static constexpr int32_t SPRS232_WAIT_RC = 2;
    static constexpr int32_t SPRS232_WAIT_RC_AFTER_DLE = 3;
    static constexpr int32_t SPRS232_WAIT_DATA = 4;
    static constexpr int32_t SPRS232_WAIT_DATA_AFTER_DLE_OR_ETX = 5;
    static constexpr int32_t SPRS232_WAIT_READ_PACKET = 6;

    // Transmission states
    static constexpr int32_t SPRS232_TX_READY = 0;
    static constexpr int32_t SPRS232_TX_WAIT_ACK = 1;

    static constexpr int32_t DLE = 0x1B;
    static constexpr int32_t STX = 0x02;
    static constexpr int32_t ETX = 0x03;
    static constexpr int32_t XON = 0x11;
    static constexpr int32_t XOFF = 0x13;

    static constexpr int32_t DIR_IN = 0x80;
    static constexpr int32_t DIR_OUT = 0x00;

    static constexpr int32_t SPRS232_ID_ID_PACKET_DATA = 0x01;
    static constexpr int32_t SPRS232_ID_ID_PACKET_ACK = 0x02;
    static constexpr int32_t SPRS232_ID_ID_PACKET_NACK = 0x04;
    static constexpr int32_t SPRS232_ID_ID_PACKET_RFU = 0x08;	/* Always 0 */
    static constexpr int32_t SPRS232_ID_MBZ = 0x10;	/* Must be ZERO to avoid ID to be egal to DLE */
    static constexpr int32_t SPRS232_ID_TYP_PACKET_LAST = 0x20;
    static constexpr int32_t SPRS232_ID_TYP_PACKET_FIRST = 0x40;
    static constexpr int32_t SPRS232_ID_TYP_PACKET_DIR = 0x80;

    static constexpr int32_t SPRS232_MASK_PACKET_TYPE = (SPRS232_ID_ID_PACKET_DATA|SPRS232_ID_ID_PACKET_ACK|SPRS232_ID_ID_PACKET_NACK|SPRS232_ID_ID_PACKET_RFU|SPRS232_ID_MBZ);

    static constexpr int32_t SPRS232_FRAME_PARTIAL = 0;
    static constexpr int32_t SPRS232_FRAME_COMPLETE = 1;

    static constexpr uint32_t SPRS232_TIMEOUT_INFINITE = 0xFFFFFFFF;
    static constexpr int32_t SPRS232_TIMEOUT_INTERVAL = 100;
    static constexpr int32_t SPRS232_TIMEOUT_ACK = 500;

    static constexpr int32_t MAX_NUMBER_OF_ATTENPT = 3;

    //from Rs232_Errors.h
    static constexpr int32_t RS232_OK = 0;
    static constexpr int32_t RS232ERR_ERROR = -520;
    static constexpr int32_t RS232ERR_BUSY = -521;
    static constexpr int32_t RS232ERR_INIT = -522;
    static constexpr int32_t RS232ERR_INVALID_HANDLE_VALUE = -523;
    static constexpr int32_t RS232ERR_IO_INCOMPLETE = -524;
    static constexpr int32_t RS232ERR_OUTSTANDING_ASYNCHRONOUS_IO_REQUEST = -525;
    static constexpr int32_t RS232ERR_IO_PENDING = -526;
    static constexpr int32_t RS232ERR_WRITE_OPERATION_ABORTED = -527;
    static constexpr int32_t RS232ERR_READ_OPERATION_ABORTED = -528;
    static constexpr int32_t RS232ERR_BADPARAM = -529;
    static constexpr int32_t RS232ERR_WRITE_CE_BREAK = -530;
    static constexpr int32_t RS232ERR_WRITE_CE_DNS = -531;
    static constexpr int32_t RS232ERR_WRITE_CE_FRAME = -532;
    static constexpr int32_t RS232ERR_WRITE_CE_IOE = -533;
    static constexpr int32_t RS232ERR_WRITE_CE_MODE = -534;
    static constexpr int32_t RS232ERR_WRITE_CE_OOP = -535;
    static constexpr int32_t RS232ERR_WRITE_CE_OVERRUN = -536;
    static constexpr int32_t RS232ERR_WRITE_CE_PTO = -537;
    static constexpr int32_t RS232ERR_WRITE_CE_RXOVER = -538;
    static constexpr int32_t RS232ERR_WRITE_CE_RXPARITY = -539;
    static constexpr int32_t RS232ERR_WRITE_CE_TXFULL = -540;
    static constexpr int32_t RS232ERR_READ_CE_BREAK = -541;
    static constexpr int32_t RS232ERR_READ_CE_DNS = -542;
    static constexpr int32_t RS232ERR_READ_CE_FRAME = -543;
    static constexpr int32_t RS232ERR_READ_CE_IOE = -544;
    static constexpr int32_t RS232ERR_READ_CE_MODE = -545;
    static constexpr int32_t RS232ERR_READ_CE_OOP = -546;
    static constexpr int32_t RS232ERR_READ_CE_OVERRUN = -547;
    static constexpr int32_t RS232ERR_READ_CE_PTO = -548;
    static constexpr int32_t RS232ERR_READ_CE_RXOVER = -549;
    static constexpr int32_t RS232ERR_READ_CE_RXPARITY = -550;
    static constexpr int32_t RS232ERR_READ_CE_TXFULL = -551;
    static constexpr int32_t RS232ERR_WRITE_TIMEOUT = -552;
    static constexpr int32_t RS232ERR_READ_TIMEOUT = -553;

    static constexpr int32_t SPRS232_OK_EOP = 1;
    static constexpr int32_t SPRS232_OK = 0;
    static constexpr int32_t SPRS232_ERR_ERROR = -560;
    static constexpr int32_t SPRS232_ERR_INIT = -561;
    static constexpr int32_t SPRS232_BAD_STUFFING = -562;
    static constexpr int32_t SPRS232_ERR_OPERATION_ABORTED = -563;
    static constexpr int32_t SPRS232_ERR_BADPARAM = -564;
    static constexpr int32_t SPRS232_ERR_BADSTX = -565;
    static constexpr int32_t SPRS232_ERR_BADDATA = -566;
    static constexpr int32_t SPRS232_ERR_BADID = -567;
    static constexpr int32_t SPRS232_ERR_CRC = -568;
    static constexpr int32_t SPRS232_ERR_NO_EOP = -569;
    static constexpr int32_t SPRS232_ERR_EOP = -570;
    static constexpr int32_t SPRS232_FRAME_NOT_COMPLETE = -571;
    static constexpr int32_t SPRS232_ERR_CONFIG_UART = -572;
    static constexpr int32_t SPRS232_ERR_SMALL_BUF = -573;

    //from SpIlv.h
    uint32_t ILV_Init(uint8_t* i_puc_ILV, uint32_t* io_ul_ILVSize, uint16_t i_us_I);
    uint32_t ILV_AddValue(uint8_t * i_puc_ILV, uint32_t* io_ul_ILVSize, uint8_t* i_puc_Value, uint32_t i_ul_ValueSize);
    uint32_t ILV_GetL(uint8_t *i_puc_ILV);
    void ILV_SetL(uint8_t* i_puc_ILV, uint32_t * io_ul_ILVSize, uint32_t i_ul_LValue);
    uint8_t	* ILV_GetV(uint8_t * i_puc_ILV);
    uint32_t ILV_GetSize(uint8_t* i_puint8_t_ILV);

    //from Crc.h
    void ComputeCrc16(uint8_t* i_puc_Buffer, uint32_t i_ul_Len, uint8_t* io_puc_CrcH, uint8_t* io_puc_CrcL);

    //from Sp_Rs232.h
    int32_t SPRS232_Initialize(int8_t *i_pc_Name, uint32_t i_ul_BaudRate);
    int32_t SPRS232_Break(uint32_t i_ul_Time);
    int32_t SPRS232_Close(void);
    int32_t SPRS232_Send(uint8_t *i_puc_Data, uint32_t i_ul_DataSize);
    int32_t SPRS232_Receive(uint8_t * i_puc_Buffer, uint32_t i_ul_BufferSize, uint32_t *o_pul_ILVSize, uint32_t i_ul_ReadTotalTimeoutConstant);
    void	s_SPRS232_MakeSOP(uint8_t l_uc_Type,uint8_t	l_b_First, uint8_t l_b_Last, uint8_t i_uc_RC, uint8_t* o_puc_PacketToSend, uint32_t* o_pul_PacketCurrentSize);
    void s_SPRS232_AddDataToPacket(uint8_t* io_puc_PacketToSend, uint32_t* io_pul_PacketCurrentSize, uint8_t* i_puc_Data, uint32_t i_ul_SizeOfSendData);
    void s_SPRS232_AddEOP(uint8_t * io_puc_PacketToSend, uint32_t * io_pul_PacketCurrentSize);
    int s_SPRS232_UnStuffing(uint8_t * o_puc_Data);
    int s_SPRS232_ReceiveSOP(uint8_t *o_uc_Type, uint8_t * o_b_First, uint8_t * o_b_Last, uint8_t *	o_puc_RC, uint32_t i_ul_ReadTotalTimeoutConstant);
    int s_SPRS232_ReadOneByte(uint8_t * o_puc_Data);
    int s_SPRS232_ReceivePacket(uint8_t * o_puc_Data, uint32_t * o_pul_Size, uint32_t i_ul_BufferSize);

    //from Drv_Rs232.h
    int32_t RS232_Initialize(int8_t *i_pc_Name, uint32_t i_ul_BaudRate);
    int32_t RS232_Close(void);
    int32_t RS232_Write(uint8_t * i_puc_Buf, uint32_t i_ul_Size, uint32_t * o_pul_BytesWritten);
    int32_t RS232_Read(uint8_t * i_puc_Buf, uint32_t i_ul_Size, uint32_t i_ul_ReadTotalTimeoutConstant);
    int32_t RS232_Break(uint32_t i_ul_Time);




};

#endif // IDEMIA_H
